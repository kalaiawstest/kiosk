import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  iconJsonArray: any[] = [{
    style: "url('../../assets/icons/wheel.ico') no-repeat;",
    content1: "Train information",
    content2: "changeContent changeContentchangeContent",
    type: "train"
  }, {
    style: "background: url('../../assets/icons/wheel.ico') no-repeat;",
    content1: "Settings information",
    content2: "changeContent changeContentchangeContent",
    type: "settings"
  }, {
    style: "background: url('../../assets/icons/wheel.ico') no-repeat;",
    content1: "Route information",
    content2: "changeContent changeContentchangeContent",
    type: "route"
  }, {
    style: "background: url('../../assets/icons/wheel.ico') no-repeat;"
  }, {
    style: "background: url('../../assets/icons/wheel.ico') no-repeat;"
  }, {
    style: "background: url('../../assets/icons/wheel.ico') no-repeat;"
  }, {
    style: "background: url('../../assets/icons/wheel.ico') no-repeat;"
  }, {
    style: "background: url('../../assets/icons/wheel.ico') no-repeat;"
  },
  ]

  constructor() { }

  public contentUpdate = new BehaviorSubject(null);

  updateContent(content) {
    this.contentUpdate.next(content);
  }

  getIconJson() {
    return this.iconJsonArray;
  }

}
