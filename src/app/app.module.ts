import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { TopComponent } from './top/top.component';
import { AltoContentComponent } from './alto-content/alto-content.component';
import { AltoIconsComponent } from './alto-icons/alto-icons.component';
import { BottomComponent } from './bottom/bottom.component';
import { ContentService } from './content.service';


@NgModule({
  declarations: [
    AppComponent,
    TopComponent,
    AltoContentComponent,
    AltoIconsComponent,
    BottomComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule
  ],
  providers: [ContentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
