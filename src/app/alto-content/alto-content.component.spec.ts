import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltoContentComponent } from './alto-content.component';

describe('AltoContentComponent', () => {
  let component: AltoContentComponent;
  let fixture: ComponentFixture<AltoContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltoContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltoContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
