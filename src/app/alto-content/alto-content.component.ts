import { Component, OnInit } from '@angular/core';
import { ContentService } from '../content.service'

@Component({
  selector: 'app-alto-content',
  templateUrl: './alto-content.component.html',
  styleUrls: ['./alto-content.component.css']
})
export class AltoContentComponent implements OnInit {


  public iconList = [
    {
      key: "train",
      icons: [
        {
          title: "To make a type specimen book", content2: "To make a type specimen book. It has survived not only five centuries",
          subIcons: [{
            icon: "circle-red fa-user-circle",
            data: "Rajan",

          }, {
            icon: "circle-red fa-user-circle",
            data: "Raja"
          }, {
            icon: "circle-red fa-user-circle",
            data: "Raja"
          }]
        }
        , {
          title: "Always free from repetition", content2: "Popular during the Renaissance. ",
          subIcons: [{
            icon: "circle-green fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-green fa-user-circle",
            data: "King"
          }, {
            icon: "circle-green fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "The standard chunk ", content2: "when an unknown printer took a galley",
          subIcons: [{
            icon: "circle-violet fa-user-circle",
            data: "Raj"
          }
          ]
        },
        {
          title: "long established fact", content2: "It is a long established fact",
          subIcons: [{
            icon: "circle-blue fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }
          ]
        }, {
          title: "The standard chunk of text", content2: "web page editors now use Lorem Ipsum as their default model",
          subIcons: [{
            icon: "circle-yellow fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-yellow fa-user-circle",
            data: "King"
          }, {
            icon: "circle-yellow fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "It is a long established", content2: "Fact that a reader will be distracted by the readable content of a page", subIcons: [{
            icon: "circle-green fa-user-circle",
            data: "Raj"
          }
          ]
        },
        {
          title: "Various versions have evolved", content2: "Handful of model sentence structures ", subIcons: [{
            icon: "circle-darkyellow fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-darkyellow fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "Predefined chunks as necessary", content2: "Predefined chunks as necessary", subIcons: [{
            icon: "circle-light-green fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-light-green fa-user-circle",
            data: "King"
          }, {
            icon: "circle-light-green fa-user-circle",
            data: "King"
          }
          ]
        },
      ]
    },
    {
      key: "settings",
      icons: [
        {
          title: "more recently with desktop publishing", content2: "To make a type specimen book. It has survived not only five centuries", subIcons: [{
            icon: "circle-green fa-user-circle",
            data: "Rajan",

          }, {
            icon: "circle-green fa-user-circle",
            data: "Raja"
          }]
        }
        , {
          title: "Repeat predefined chunks ", content2: "Popular during the Renaissance. ", subIcons: [{
            icon: "circle-blue fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "Various versions have evolved ", content2: "when an unknown printer took a galley", subIcons: [{
            icon: "circle-red fa-user-circle",
            data: "Raj"
          }
          ]
        },
        {
          title: "use a passage of text", content2: "It is a long established fact", subIcons: [{
            icon: "circle-red fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-red fa-user-circle",
            data: "King"
          }, {
            icon: "circle-red fa-user-circle",
            data: "King"
          }, {
            icon: "circle-red fa-user-circle",
            data: "King"
          }
          ]
        }, {
          title: "publishing packages", content2: "web page editors now use Lorem Ipsum as their default model", subIcons: [{
            icon: "circle-darkyellow fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-darkyellow fa-user-circle",
            data: "King"
          }, {
            icon: "circle-darkyellow fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "handful of model sentence", content2: "Train Information ", subIcons: [{
            icon: "circle-yellow fa-user-circle",
            data: "Raj"
          }
          ]
        },
        {
          title: "combined with a handful of model", content2: "Train Information ", subIcons: [{
            icon: "circle-green fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-green fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "Trandomised words of text", content2: "Train Information ", subIcons: [{
            icon: "circle-red fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-red fa-user-circle",
            data: "King"
          }, {
            icon: "circle-red fa-user-circle",
            data: "King"
          }
          ]
        },
      ]
    },
    {
      key: "route",
      icons: [
        {
          title: "To make a type specimen book", content2: "To make a type specimen book. It has survived not only five centuries", subIcons: [{
            icon: "circle-red fa-user-circle",
            data: "Rajan",

          }, {
            icon: "circle-red fa-user-circle",
            data: "Raja"
          }, {
            icon: "circle-red fa-user-circle",
            data: "Raja"
          }]
        }
        , {
          title: "The first line of", content2: "Popular during the Renaissance. ", subIcons: [{
            icon: "circle-green fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-green fa-user-circle",
            data: "King"
          }, {
            icon: "circle-green fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "The standard chunk ", content2: "when an unknown printer took a galley", subIcons: [{
            icon: "circle-violet fa-user-circle",
            data: "Raj"
          }
          ]
        },
        {
          title: "long established fact", content2: "It is a long established fact", subIcons: [{
            icon: "circle-blue fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }
          ]
        }, {
          title: "publishing packages", content2: "web page editors now use Lorem Ipsum as their default model", subIcons: [{
            icon: "circle-yellow fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-yellow fa-user-circle",
            data: "King"
          }, {
            icon: "circle-yellow fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: " Title Info 3", content2: "Train Information ", subIcons: [{
            icon: "circle-green fa-user-circle",
            data: "Raj"
          }
          ]
        },
        {
          title: "The standard chunk of text", content2: "Train Information ", subIcons: [{
            icon: "circle-darkyellow fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-darkyellow fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "Title text Information", content2: "Train Information ", subIcons: [{
            icon: "circle-light-green fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-light-green fa-user-circle",
            data: "King"
          }, {
            icon: "circle-light-green fa-user-circle",
            data: "King"
          }
          ]
        },
      ]
    },
    {
      key: "booking information",
      icons: [
        {
          title: "To make a type specimen book", content2: "To make a type specimen book. It has survived not only five centuries", subIcons: [{
            icon: "circle-red fa-user-circle",
            data: "Rajan",

          }, {
            icon: "circle-red fa-user-circle",
            data: "Raja"
          }, {
            icon: "circle-red fa-user-circle",
            data: "Raja"
          }]
        }
        , {
          title: "Always free from repetition", content2: "Popular during the Renaissance. ", subIcons: [{
            icon: "circle-green fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-green fa-user-circle",
            data: "King"
          }, {
            icon: "circle-green fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "The standard chunk ", content2: "when an unknown printer took a galley", subIcons: [{
            icon: "circle-violet fa-user-circle",
            data: "Raj"
          }
          ]
        },
        {
          title: "long established fact", content2: "It is a long established fact", subIcons: [{
            icon: "circle-blue fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }, {
            icon: "circle-blue fa-user-circle",
            data: "King"
          }
          ]
        }, {
          title: "publishing packages", content2: "web page editors now use Lorem Ipsum as their default model", subIcons: [{
            icon: "circle-yellow fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-yellow fa-user-circle",
            data: "King"
          }, {
            icon: "circle-yellow fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "Train Title Info 3", content2: "Train Information ", subIcons: [{
            icon: "circle-green fa-user-circle",
            data: "Raj"
          }
          ]
        },
        {
          title: "Train Title Info 3", content2: "Train Information ", subIcons: [{
            icon: "circle-darkyellow fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-darkyellow fa-user-circle",
            data: "King"
          }
          ]
        },
        {
          title: "Train Title Info 3", content2: "Train Information ", subIcons: [{
            icon: "circle-light-green fa-user-circle",
            data: "Raj"
          }, {
            icon: "circle-light-green fa-user-circle",
            data: "King"
          }, {
            icon: "circle-light-green fa-user-circle",
            data: "King"
          }
          ]
        },
      ]
    }];
  activeIcon = { "icons": [] };
  data = "";
  constructor(private contentService: ContentService) {
    this.contentService.contentUpdate.subscribe(data => {
      if (data) {
        this.activeIcon = this.iconList.find(x => x.key === data.type);
      }
    })
  }
  showData(data) {
    this.data = data;
  }

  showContentData(data) {
    this.data = data;
  }


  ngOnInit() {
  }

}
