import { Component, OnInit } from '@angular/core';
import { ContentService } from '../content.service'

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.css']
})
export class TopComponent implements OnInit {

  content: any = {};
  constructor(private contentService: ContentService) {
    this.contentService.contentUpdate.subscribe(data => {
      this.content = data;
    })
  }

  ngOnInit() {
  }

}
