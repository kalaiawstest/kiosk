import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { ContentService } from './content.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'altoangular';
  iconJsonArray = [];
  constructor(private contentService: ContentService, private chRef: ChangeDetectorRef) {
    this.iconJsonArray = this.contentService.getIconJson();
  }

  ngAfterViewInit() {
    this.contentService.contentUpdate.next(this.iconJsonArray[0]);
    this.chRef.detectChanges();
  }
}
