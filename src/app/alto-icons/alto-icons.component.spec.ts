import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltoIconsComponent } from './alto-icons.component';

describe('AltoIconsComponent', () => {
  let component: AltoIconsComponent;
  let fixture: ComponentFixture<AltoIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltoIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltoIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
