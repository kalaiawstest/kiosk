import { Component, OnInit } from '@angular/core';
import { ContentService } from '../content.service';
declare let $: any;
@Component({
  selector: 'app-alto-icons',
  templateUrl: './alto-icons.component.html',
  styleUrls: ['./alto-icons.component.css']
})
export class AltoIconsComponent implements OnInit {

  iconJsonArray = [];
  constructor(private contentService: ContentService) {
    this.iconJsonArray = this.contentService.getIconJson();
  }

  ngOnInit() {

    $('#recipeCarousel').carousel({
      interval: 10000
    })

    $('.carousel .carousel-item').each(function () {
      var next = $(this).next();
      if (!next.length) {
        next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));

      if (next.next().length > 0) {
        next.next().children(':first-child').clone().appendTo($(this));
      }
      else {
        $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
      }
    });

  }

  changeContent(i) {

    this.contentService.updateContent(this.iconJsonArray[i])
  }



}
